# ShedLock
依赖如下依赖即可:
```
<dependency>
    <groupId>com.yc.framework</groupId>
    <artifactId>yc-common-shedlock</artifactId>
</dependency>
```

源代码示例:
https://github.com/developers-youcong/yc-framework/tree/main/yc-example/yc-example-shedlock

# 博客文章
[深入浅出分布式定时任务之ShedLock](https://youcongtech.com/2022/04/16/%E6%B7%B1%E5%85%A5%E6%B5%85%E5%87%BA%E5%88%86%E5%B8%83%E5%BC%8F%E5%AE%9A%E6%97%B6%E4%BB%BB%E5%8A%A1%E4%B9%8BShedLock/)